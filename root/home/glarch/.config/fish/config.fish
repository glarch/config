# anaconda
# set -x PATH /opt/anaconda/bin $PATH

# CUDA
# set -x PATH /opt/cuda/bin $PATH

# fcitx
# set -x XMODIFIERS=@im fcitx
# set -x GTK_IM_MODULE fcitx
# set -x QT_IM_MODULE fcitx

# ibus
set -x GTK_IM_MODULE ibus
set -x XMODIFIERS @im ibus
set -x QT_IM_MODULE ibus

# powerline-go
# function fish_prompt
#     powerline-go -error $status -shell bare
# end

# powerline-rs
function fish_prompt
    powerline-rs --shell bare $status
end

# alias
alias update-grub 'sudo grub-mkconfig -o /boot/grub/grub.cfg'
alias gpg 'gpg2'
alias gti 'git'

funcsave update-grub
funcsave gpg
funcsave gti
